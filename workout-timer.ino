// Backpack dependencies
#include <Adafruit_GFX.h>
#include <Adafruit_SPITFT_Macros.h>
#include <Adafruit_SPITFT.h>
#include <gfxfont.h>

#include <Adafruit_LEDBackpack.h>

#include <math.h> // For floor()

/*
 * workout-timer
 * -------------
 * Coach Preston has a workout timer he uses in 523 Bootcamp. This is an
 * Arduino-based adaptation of that timer.
 *
 * TODO/ISSUES:
 * - Once the time is set, if you go to set time again, the up button doesn't work.
 *
 * Connections:
 * - Buttons:
 *   - Connect pins according to button layout (below) to top right of button
 *   - 10k ohm resistor from bottom right of button to GND
 *   - 5V to bottom left of button
 * - Speaker:
 *   - Connect pin 10 to long lead (+) of piezo
 *   - Connect GND to short lead (-)
 * - Display:
 *   - SDA to SDA
 *   - SCL to SCL
 *   - 5V to 5V
 *   - GND to GND
 *   - Jumper from 5V to V_IC
 */

/*
 * Assign button pins accordingly
 *   2. Round time      3. Rest time
 *   4. Interval time   5. Rounds
 *   6. Up              7. Down
 *   8. Start           9. Stop
 */
const int round_time_button = 2;
const int rest_time_button = 3;
const int interval_time_button = 4;
const int rounds_button = 5;
const int up_button = 6;
const int down_button = 7;
const int start_button = 8;
const int stop_button = 9;

// Piezo speaker output pin
const int buzzer = 10;

// Initialize a new matrix display object.
Adafruit_7segment matrix = Adafruit_7segment();

/*
 * Whether the button has been pressed for:
 * [0]ROUND, [1]REST, [2]INTERVAL, [3]ROUNDS.
 */
bool button_active[4] = {false, false, false, false};

/*
 * Whether the button has been long pressed for:
 * [0]ROUND, [1]REST, [2]INTERVAL, [3]ROUNDS.
 */
bool button_long_press_active[4] = {false, false, false, false};

// Time for each time type: [0]ROUND, [1]REST, [2]INTERVAL, [3]ROUNDS.
int event_times[4] = {0, 0, 0, 0};

/*
 * Timers. The millis() function returns an `unsigned long`. We likewise use
 * this to avoid arithmetic errors.
 */
unsigned long button_start_time = 0;
unsigned long long_press_time = 500;
unsigned long countdown_start_time = 0;

// Whether or not the countdown is running.
bool countdown_allowed = false;

/*
 * Function: minutes
 * -----------------
 * Returns the number of minutes in a given number of seconds.
 *
 * time: the supplied time (in seconds).
 *
 * returns: integer number of minutes in the given time.
 */
int minutes(int time)
{
  return (int) floor(time / 60);
}

/*
 * Function: ten_seconds
 * -----------------
 * Returns the tens (second) digit for the given time.
 *
 * time: the supplied time (in seconds).
 *
 * returns: the tens (second) digit.
 */
int ten_seconds(int time)
{
  return (int) floor((time % 60) / 10);
}

/*
 * Function: seconds
 * -----------------
 * Returns the single digit seconds in a given time.
 *
 * time: the supplied time (in seconds).
 *
 * returns: the single digit seconds.
 */
int seconds(int time)
{
  return time % 10;
}

/*
 * Function: display_time
 * ----------------------
 * Converts the supplied time to minutes and seconds and displays it on the
 * seven-segment display.
 *
 * time: the supplied time (in seconds)
 */
void display_time(int time)
{
  matrix.writeDigitNum(1, minutes(time), true);
  matrix.writeDigitNum(3, ten_seconds(time), true);
  matrix.writeDigitNum(4, seconds(time), true);

  matrix.drawColon(true);
  matrix.writeDisplay();
}

/*
 * Function: display_rounds
 * ------------------------
 * Display a small 'r' and then the number of rounds set.
 */
void display_rounds(int rounds)
{
  // matrix.print('r', DEC);
  matrix.print(0xBEEF, HEX);
  matrix.drawColon(false);
  matrix.writeDisplay();
}

/*
 * Function: sound_off
 * -------------------
 * Sound the buzzer $count times and flash the display.
 *
 * count: the number of times to sound the buzzer.
 * delay: the length of the buzz and the delay between buzzes.
 */
void sound_off(int count, int delay_length)
{
  for ( int i = 0; i < count; i++ )
  {
    tone(buzzer, 1000); // send 1kHz sound signal
    delay(delay_length);
    noTone(buzzer);
    delay(delay_length);
  }
}

/*
 * Function: check_for_button_press
 * --------------------------------
 * Detect if the button has been pressed or long pressed. Short presses happen
 * on button release. Long presses happen after $long_press_time.
 *
 * Button notes:
 * - When the button is open (unpressed), it's connected to GND and reads LOW.
 * - When the button is closed (pressed), it's connected to 5V and reads HIGH.
 *
 * button_type: an integer dictating the button_type:
 *   2. Round time      3. Rest time
 *   4. Interval time   5. Rounds
 *   6. Up              7. Down
 *   8. Start           9. Stop
 *
 * returns: 2 for long press, 3 for short press.
 */
int check_for_button_press(int button_type)
{
  while ( digitalRead(button_type) == HIGH ) // button pressed
  {
    if ( button_active[button_type] == false ) // from initialization or having run before
    {
      button_active[button_type] = true;
      button_start_time = millis();
    }

    if ( millis() - button_start_time >= long_press_time
          && button_long_press_active[button_type] == false )
    {
      button_long_press_active[button_type] = true;

      // perform action
      sound_off(1, 100);
      return 2;
    }
  }

  if ( button_active[button_type] ) // code is looping for first time since button release
  {
    button_active[button_type] = false; // allow button to be pressed again

    if ( button_long_press_active[button_type] )
    {
      // allow long press to happen again later
      button_long_press_active[button_type] = false;
      return 1;
    }
    else // button truly released
    {
      // perform action
      Serial.print("##### In function check_for_button_press(");
      Serial.print(button_type);
      Serial.println(") #####");
      sound_off(1, 100);
      return 3;
    }
  }

  return 4;
}

/*
 * Function: start_button_pressed
 * ---------------------------------
 * When the START button is pressed, sound_off, allow the countdown to start,
 * and mark the time the event happened at.
 */
void start_button_pressed()
{
  if ( start_button == HIGH )
  {
    sound_off(3, 200);
    countdown_allowed = true;
    countdown_start_time = millis();
  }

  while ( countdown_allowed && event_times[3] > 0 )
  {
    if ( stop_button == HIGH )
    {
      display_time(event_times[0]);
      countdown_allowed = false;
      break;
    }

    // Countdown round time and sound_off on interval.
    if ( event_times[2] != 0 ) // Avoid division by 0 error.
    {
      if ( event_times[0] % event_times[2] == 0 )
      {
        sound_off(2, 300);
      }
    }

    // sound_off when that's done
    // sound_off(5, 100);
    // run through rest time
    event_times[3]--;
    // PREVIOUS CODE (from loop()):
    // If there is at least 1 second left in the countdown, decrement one second
    // and display the new time. Don't stop until the stop button is pressed.
    countdown(event_times[0]);

    // When the countdown finishes: sound_off, run through the rest time, and
    // loop back to round time.
    countdown_complete(event_times[0]);
  }
}

/*
 * Function: countdown
 * -------------------
 * While the countdown is allowed and there is at least one second left,
 * decrement the round time and then display it. Only the stop button can end
 * the countdown.
 *
 * time: the time to countdown (in seconds).
 */
void countdown(int time)
{
  if ( millis() - countdown_start_time >= 1000 )
  {
    time--;
    display_time(time);
  }
}

/*
 * Function: countdown_complete
 * ----------------------------
 * When the countdown is complete, sound_off and kill the timer.
 *
 * time: which time to check completion on (in seconds).
 */
void countdown_complete(int time)
{
  if ( time == 0 )
  {
    sound_off(4, 200);
    countdown_allowed = false;
    display_time(0);
  }
}

/*
 * Function: function_button_pressed
 * ---------------------------------
 * On short press: display time.
 * On long press: flash time and allow it to be incremented and decremented
 * according to its context.
 */
void function_button_pressed(int context)
{
  int time_context = context - 2; // convert pin number to event_time context.

  /* Calling the check_for_button_press() function multiple times allows for
   * more than one button press to be detected or for us to miss a press.
   */
  int button_return = check_for_button_press(context);

  if ( button_return == 3 ) // short press
  {
    Serial.print("##### Function button ");
    Serial.print(context);
    Serial.println(" pressed #####");

    if ( context == 5 ) // rounds
    {
      display_rounds(event_times[time_context]);
    }
    else
    {
      display_time(event_times[time_context]);
    }
  }
  else if ( button_return == 2 ) // long press
  {
    matrix.blinkRate(2);
    matrix.writeDisplay();

    do
    {
      up_button_pressed(time_context);
      down_button_pressed(time_context);

      if ( context == 5 ) // rounds
      {
        display_rounds(event_times[time_context]);
      }
      else
      {
        display_time(event_times[time_context]);
      }

      button_return = check_for_button_press(context);
    } while ( button_return != 3 ); // wait for short press

    matrix.blinkRate(0);
    matrix.writeDisplay();
  }
}

/*
 * Function: up_button_pressed
 * ---------------------------
 * When the up button is pressed, increment the specified time.
 *
 * context: the event_time to increment.
 */
void up_button_pressed(int context)
{
  if ( check_for_button_press(6) == 3 )
  {
    switch ( context )
    {
      case 0: // round time
        if ( event_times[context] == 540 ) // if we're at 9 minutes
        {
          event_times[context] = 60;
        }
        else
        {
          event_times[context] += 60;
        }
        break;

      case 1: // rest time
      case 2: // interval time
        if ( event_times[context] == 60 )
        {
          event_times[context] = 5;
        }
        else
        {
          event_times[context] += 5;
        }
        break;

      case 3: // rounds
        if ( event_times[context] == 99 )
        {
          event_times[context] = 1;
        }
        else
        {
          event_times[context] += 1;
        }
        break;

      default:
        break;
    }
  }
}

/*
 * Function: down_button_pressed
 * -----------------------------
 * When the down button is pressed, decrement the specified time.
 *
 * context: the event_time to decrement.
 */
void down_button_pressed(int context)
{
  if ( check_for_button_press(7) == 3 )
  {
    switch ( context )
    {
      case 0:
        if ( event_times[context] == 60 )
        {
          event_times[context] = 540; // loop to 9 minutes
        }
        else
        {
          event_times[context] -= 60;
        }
        break;

      case 1: // rest time
      case 2: // interval time
        if ( event_times[context] == 5 )
        {
          event_times[context] = 60;
        }
        else
        {
          event_times[context] -= 5;
        }
        break;

      case 3: // rounds
        if ( event_times[context] == 1 )
        {
          event_times[context] = 99;
        }
        else
        {
          event_times[context] -= 1;
        }
        break;

      default:
        break;
    }
  }
}

/*
 * Function: setup
 * ---------------
 * Initialize button switches, speaker, and display.
 */
void setup()
{
  Serial.begin(115200);

  pinMode(round_time_button, INPUT);
  pinMode(rest_time_button, INPUT);
  pinMode(interval_time_button, INPUT);
  pinMode(rounds_button, INPUT);
  pinMode(up_button, INPUT);
  pinMode(down_button, INPUT);
  pinMode(start_button, INPUT);
  pinMode(stop_button, INPUT);

  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(buzzer, OUTPUT);

  // Turn on the display and show the round time (00:00).
  matrix.begin(0x70);
  matrix.setBrightness(15); // 0-15
  display_time(event_times[0]);
}

void loop()
{
  function_button_pressed(2); // round time
  function_button_pressed(3); // rest time
  function_button_pressed(4); // interval time
  function_button_pressed(5); // rounds

  if ( digitalRead(interval_time_button) == HIGH )
  {
    Serial.println("interval time button hit");
  }

  if ( digitalRead(rounds_button) == HIGH )
  {
    Serial.println("rounds button hit");
  }

  // If nothing is being set, the up and down buttons control the volume (four settings)

  // If the countdown time is less than thirty seconds, flash the display every second.

  // When the start button is pressed: start countdown and mark the time.
  start_button_pressed();
}
