# workout-timer
An Arduino-based copy of the workout timer Coach Preston has.

## Physical layout
The timer looks like this (the words on the right are button labels):
```
 _____________________________________________
|   __   __      __   __                      |
|  |  | |  |  . |  | |  |    ROUND     REST   |
|  |__| |__|    |__| |__|    INTERVAL  ROUNDS |
|  |  | |  |  . |  | |  |    UP        DOWN   |
|  |__|.|__|.   |__|.|__|.   START     STOP   |
|_____________________________________________|
```

## Usage
| Button     | Short press                          | Long press                            |
| ---------- | ------------------------------------ | ------------------------------------- |
| `ROUND`    | Display the current round time       | **\*flashing\*** Set round time       |
| `REST`     | Display the current rest period time | **\*flashing\*** Set rest time        |
| `INTERVAL` | Display the current interval time    | **\*flashing\*** Set interval time    |
| `ROUNDS`   | Display the current number of rounds | **\*flashing\*** Set number of rounds |
| `UP`       | Increment time/volume                | -                                     |
| `DOWN`     | Decrement time/volume                | -                                     |
| `START`    | Begin countdown                      | -                                     |
| `STOP`     | Stop countdown, display round time   | -                                     |

## References
- [Button tutorial](https://www.arduino.cc/en/tutorial/button)
- [Long press button](https://www.instructables.com/id/Arduino-Dual-Function-Button-Long-PressShort-Press/)
- [Seven-segment display](https://learn.adafruit.com/adafruit-led-backpack/1-2-inch-7-segment-backpack)
